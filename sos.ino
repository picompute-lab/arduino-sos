/*
  SOS - MORSE
 
 International Morse code details: http://en.wikipedia.org/wiki/Morse_code
 */

// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 13;

int UNIT = 200;

int DASH_WIDTH = 3 * UNIT;
int DOT_WIDTH  = 1 * UNIT;
int SPACE_BETWEEN_PARTS = 1 * UNIT;

int SPACE_BETWEEN_LETTERS = 3 * UNIT;
int SPACE_BETWEEN_WORDS   = 7 * UNIT;

const char* MORSE_CODE_LETTERS[26] = {
  /* A */  ".-",
  /* B */  "-...",
  /* C */  "-.-.",
  /* D */  "-..",
  /* E */  ".",
  /* F */  "..-.",
  /* G */  "--.",
  /* H */  "....",
  /* I */  "..",
  /* J */  ".---",
  /* K */  "-.-",
  /* L */  ".-..",
  /* M */  "--",
  /* N */  "-.",
  /* O */  "---",
  /* P */  ".--.",
  /* Q */  "--.-",
  /* R */  ".-.",
  /* S */  "...",
  /* T */  "-",
  /* U */  "..-",
  /* V */  "...-",
  /* W */  ".--",
  /* X */  "-..-",
  /* Y */  "-.--",
  /* Z */  "--.."
};

const char* MORSE_CODE_NUMBERS[10] = {
  /* 0 */  "-----",  
  /* 1 */  ".----",
  /* 2 */  "..---",
  /* 3 */  "...--",
  /* 4 */  "....-",
  /* 5 */  ".....",
  /* 6 */  "-....",
  /* 7 */  "--...",
  /* 8 */  "---..",
  /* 9 */  "----."
};

const char* morse_for(const char c) { 
  if (c >= 'A' && c <= 'Z')
    return MORSE_CODE_LETTERS[c - 'A'];
  else if (c >= '0' && c <= '9')
    return MORSE_CODE_NUMBERS[c - 'A'];

  // Error condition
  return MORSE_CODE_NUMBERS[0];
};

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);     
}

void space(int space_width) {
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(space_width);
}

void dash() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(DASH_WIDTH);
}

void dot() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(DOT_WIDTH);
}

void show_letter(const char* morse)
{ 
  int len = strlen(morse);
  for (int i = 0; i< len; i++)
  {
    morse[i] == '.' ? dot() : dash();

    if (i < len - 1)
    {
      space(SPACE_BETWEEN_PARTS);       
    }
  }
}

void show_word(const char* w)
{
  int len = strlen(w);
  for (int i = 0; i< len; i++)
  {
    show_letter(morse_for(w[i]));

    if (i < len - 1)
    {
      space(SPACE_BETWEEN_LETTERS);       
    }
  }

  space(SPACE_BETWEEN_WORDS);
}

// the loop routine runs over and over again forever:
void loop() {
  show_word("SOS");
}
